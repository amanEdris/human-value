/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuseki.query.controller;

import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.sparql.engine.binding.Binding;
import fuseki.query.service.FusekiService;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author edris
 */
@WebServlet(name = "QueryController", urlPatterns = {"/queryCatalog"})
public class QueryController extends HttpServlet {

    /**
     * Handles the HTTP Get
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Handles the query posted by the client
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String queryString = request.getParameter("queryString");
        String sparqlEndPointURL = request.getParameter("endpoint");
        String returnTable = null;
        String error = null;

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            FusekiService fs = new FusekiService(sparqlEndPointURL, queryString);
            if (fs.executeQuery() instanceof Model) {
                Model m = (Model) fs.executeQuery();
                m.write(baos, "TTL");
                returnTable = baos.toString();
            }else if (fs.executeQuery() instanceof Boolean) {
                Boolean results =  (Boolean) fs.executeQuery();
                ResultSetFormatter.out(baos, results);
                returnTable = baos.toString();
            }else {
                ResultSet results = (ResultSet) fs.executeQuery();
                ResultSetFormatter.outputAsJSON(baos, results);
                String json = new String(baos.toByteArray());
                returnTable = json;
                //@todo process json to turtle format 
            }

        } catch (Exception ex) {
            Logger.getLogger(QueryController.class.getName()).log(Level.SEVERE, null, ex);
            error = ex.getLocalizedMessage();

        }

        queryString = queryString.substring(queryString.lastIndexOf("#>") + 2);

        request.setAttribute("queryString", queryString);
        request.setAttribute("result", returnTable);
        request.setAttribute("error", error);
        RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
        rd.forward(request, response);

    }

}
