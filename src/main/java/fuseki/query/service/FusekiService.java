/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuseki.query.service;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.Syntax;
import com.hp.hpl.jena.rdf.model.Model;
import org.apache.jena.query.ParameterizedSparqlString;
import static org.apache.jena.sparql.lang.ParserBase.throwParseException;
import org.apache.jena.update.UpdateExecutionFactory;

/**
 * Handles all types of query to Fuseki server
 *
 * @author edris
 */
public class FusekiService {

    private Query query;

    /**
     * Enumeration for all type of queries from client
     */
    enum QueryType {
        SELECT, CONSTRUCT, DESCRIBE, ASK, UPDATE
    }

    /**
     *
     * @param sp sparql end point string URI excluding query and update
     * @param q query string from user
     */
    public FusekiService(String sp, String q) {
        FusekiUtil.setQueryString(q);
        FusekiUtil.setSparqlEndPoint(sp);
        query = QueryFactory.create(FusekiUtil.getQueryString(), Syntax.syntaxARQ);
    }

    /**
     * Execute the query
     *
     * @return
     * @throws Exception
     */
    public Object executeQuery() throws Exception {
        switch (getQueryType(query)) {
            case SELECT:
                return queryFUSEKI();
            case CONSTRUCT:
                return constructFUSEKI();
            case DESCRIBE:
                return describeFUSEKI();
            case ASK:
                return askFUSEKI();
            case UPDATE:
                updateFUSEKI();
                break;
            default:
                return "";
        }
        return null;
    }

    /**
     * Method for Select query
     *
     * @return resultSet
     * @throws Exception
     */
    public ResultSet queryFUSEKI() throws Exception {
        if (query.isSelectType() || query.isDistinct()) {
            QueryExecution qe = QueryExecutionFactory.sparqlService(FusekiUtil.getQUERY_FUSEKI_CLIENT_URL(),
                    FusekiUtil.getQueryString());
            ResultSet results = qe.execSelect();
            return results;

        } else {
            throwParseException("query not a SELECT query" + FusekiUtil.getQueryString());
        }
        return null;
    }

    /**
     * Method for ASK query
     *
     * @return boolean
     */
    public boolean askFUSEKI() throws Exception {
        if (!query.isAskType()) {
            throwParseException("query not a ASK query" + FusekiUtil.getQueryString());
        }
        QueryExecution qe = QueryExecutionFactory.sparqlService(FusekiUtil.getQUERY_FUSEKI_CLIENT_URL(),
                FusekiUtil.getQueryString());
        boolean results = qe.execAsk();
        return results;
    }

    /**
     * Method for UPDATE query
     *
     * @param updateQuery
     * @throws Exception
     */
    public static void updateFUSEKI() throws Exception {
        ParameterizedSparqlString s = new ParameterizedSparqlString();
        s.setCommandText(FusekiUtil.getQueryString());
        org.apache.jena.update.UpdateRequest update = s.asUpdate();
        org.apache.jena.update.UpdateProcessor proc = UpdateExecutionFactory.createRemote(update, FusekiUtil.getUPDATE_FUSEKI_CLIENT_URL());
        proc.execute();
    }

    /**
     * Method for construct query
     *
     * @param constructQuery
     * @return
     * @throws Exception
     */
    public Model constructFUSEKI() throws Exception {
        if (!query.isConstructType()) {
            throwParseException("query not a CONSTRUCT query" + FusekiUtil.getQueryString());
        }
        QueryExecution qe = QueryExecutionFactory.sparqlService(FusekiUtil.getQUERY_FUSEKI_CLIENT_URL(),
                FusekiUtil.getQueryString());
        Model results = qe.execConstruct();

        return results;
    }

    /**
     * Method to handle describe queries
     *
     * @return
     * @throws Exception
     */
    public Model describeFUSEKI() throws Exception {
        if (!query.isDescribeType()) {
            throwParseException("query not a DESCRIBE query" + FusekiUtil.getQueryString());
        }
        QueryExecution qe = QueryExecutionFactory.sparqlService(FusekiUtil.getQUERY_FUSEKI_CLIENT_URL(),
                FusekiUtil.getQueryString());
        Model results = qe.execDescribe();

        return results;
    }

    //return the query type
    private static FusekiService.QueryType getQueryType(final Query query) {
        if (query.isSelectType() || query.isDistinct()) {
            return FusekiService.QueryType.SELECT;
        }
        if (query.isConstructType()) {
            return FusekiService.QueryType.CONSTRUCT;
        }
        if (query.isDescribeType()) {
            return FusekiService.QueryType.DESCRIBE;
        }
        if (query.isAskType()) {
            return FusekiService.QueryType.ASK;
        }
        if (query.isUnknownType()) {
            return FusekiService.QueryType.UPDATE;
        }
        return null;
    }


}
