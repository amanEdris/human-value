/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fuseki.query.service;

/**
 *
 * Helper Class for configuring  SPARQL End point and User query
 */
public class FusekiUtil {

    private static String  sparqlEndPoint;
    private static String queryString;
    public static final String QUERY_FUSEKI_CLIENT_URL = "/query";
    public static final String UPDATE_FUSEKI_CLIENT_URL = "/update";

    /**
     * Method for SELECT, ASK query SPARQL End point
     * @return 
     */
    public static String getQUERY_FUSEKI_CLIENT_URL() {
        return sparqlEndPoint+QUERY_FUSEKI_CLIENT_URL;
    }
    
    /**
     * Method for update query SPARQL End point
     * @return 
     */
    public static String getUPDATE_FUSEKI_CLIENT_URL() {
        return sparqlEndPoint+UPDATE_FUSEKI_CLIENT_URL;
    }

    public static String getSparqlEndPoint() {
        return sparqlEndPoint;
    }

    public static void setSparqlEndPoint(String sparqlEndPoint) {
        FusekiUtil.sparqlEndPoint = sparqlEndPoint;
    }

    public static String getQueryString() {
        return queryString;
    }

    public static void setQueryString(String queryString) {
        FusekiUtil.queryString = queryString;
    }


}
