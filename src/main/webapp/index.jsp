<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body style="">
      
        <h1>      Values Map Repository</h1>
        <hr/>
        <form method="post" action="queryCatalog">
            <table border="0" cellpadding="2">
                <tbody>
                    <tr>
                        <td> 
                            <h4>Query:</h4>Sparql end point<input size="25"  type="text" name="endpoint" value="http://localhost:3030/valueMap">
                        </td>
                        <td>
                            <textarea  name="queryString" rows="16" cols="80">
PREFIX :<http://www.semanticweb.org/ujwal/Values-Map-Ontology#>
PREFIX owl:<http://www.w3.org/2002/07/owl#>
PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xml:<http://www.w3.org/XML/1998/namespace>
PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
PREFIX gdpr: <http://www.semanticweb.org/ujwal/GDPR-Ontology#>
PREFIX hv: <http://www.semanticweb.org/ujwal/Human-Values-Ontology#>${requestScope.queryString}
                            </textarea>
                            <input type="submit"/> 
                        </td>
                    </tr>
                    <tr>
                        <td><h4>Results:</h4> </td>
                        <td>
                            <b style="color:red">${requestScope.error}</b>${requestScope.result}
                        </td>
                    </tr>
                </tbody>
            </table>

        </form>
    </body>
</html>
